import 'dart:convert';
import 'dart:html';

import 'package:excel/excel.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Stopwatch stopwatch = new Stopwatch()..start();

            Excel excel = Excel.createExcel();
            Sheet sh = excel['Sheet1'];
            for (int i = 0; i < 8; i++) {
              sh.cell(CellIndex.indexByColumnRow(rowIndex: 0, columnIndex: i))
                  .value = 'Col $i';
              sh.cell(CellIndex.indexByColumnRow(rowIndex: 0, columnIndex: i))
                  .cellStyle = CellStyle(bold: true);
            }
            for (int row = 1; row < 9000; row++) {
              for (int col = 0; col < 9; col++) {
                sh.cell(CellIndex.indexByColumnRow(columnIndex: col, rowIndex: row))
                    .value = 'value ${row}_$col';
              }
            }
            print('Generating executed in ${stopwatch.elapsed}');
            stopwatch.reset();
            excel.encode().then((onValue) {
              print('Encoding executed in ${stopwatch.elapsed}');
              stopwatch.reset();
              downloadFile(onValue, 'excel_test.xlsx');
              print('Downloaded executed in ${stopwatch.elapsed}');
            });
          },
          child: Text('Export'),
        ),
      ),
    );
  }

  void downloadFile(List<int> bytes, String downloadName) {
    // Encode our file in base64
    final _base64 = base64Encode(bytes);
    // Create the link with the file
    final anchor =
    AnchorElement(href: 'data:application/octet-stream;base64,$_base64')
      ..target = 'blank'
      ..setAttribute('download', downloadName);
    // add the name
    //anchor.download = downloadName;
    // trigger download
    document.body.append(anchor);
    anchor.click();
    anchor.remove();
    return;
  }
}
